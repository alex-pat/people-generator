#!/usr/bin/env ruby
# encoding: utf-8

require "rainbow/ext/string"
require "parallel"
require_relative "ffaker_extended.rb"

class PeopleGenerator
  REGIONS = { :US => { :name => FFaker::Name,   :address => FFaker::Address   },
              :RU => { :name => FFaker::NameRU, :address => FFaker::AddressRU },
              :BY => { :name => FFaker::NameBY, :address => FFaker::AddressBY } }.freeze

  ALPHABET = ("a".."z").to_a.freeze

  PROCESS_COUNT = 4.freeze

  ERROR_FUNCTIONS = [
      lambda do |human|
        i = rand(human.length)
        human.delete! human[i]
      end,

      lambda do |human|
        human.insert rand(human.length), ALPHABET.sample
      end,

      lambda do |human|
        length = human.length
        i = rand(length - 1)
        human[i], human[i + 1] = human[i + 1], human[i]
      end
  ].freeze

  def initialize
    input_error if ARGV.length != 3
    parse_args
    @region[:address].load_db

    people_per_proc = get_people_per_proc
    Parallel.each(people_per_proc, :in_processes => 4) do |count|
      count.times do
        human = get_correct_people
        ERROR_FUNCTIONS.sample.call human if rand < @err_chance
        puts human
      end
    end
  end

  def parse_args
    @region = REGIONS[ARGV[0].intern]
    @people_count = ARGV[1].to_i
    @err_chance = ARGV[2].to_f

    input_error if args_wrong? @people_count, @region, @err_chance
  end

  def get_correct_people
    "#{@region[:name].name}, #{@region[:address].street_address}, #{@region[:address].city_zip_code}, #{@region[:address].phone}"
  end

  def args_wrong? (people_count, region, err_chance)
    people_count < 1 || region == nil || err_chance < 0 || err_chance > 1
  end

  def get_people_per_proc
    people_per_proc = [@people_count / PROCESS_COUNT] * PROCESS_COUNT
    people_per_proc[0] += @people_count % PROCESS_COUNT
    people_per_proc
  end

  def input_error
    $stderr.puts <<-INFO
  Input error
  Usage:
  #{ "[" + "ruby".bright + "] " +
        __FILE__.bright +
        " {" + "US".bright + " | " + "RU".bright + " | " + "BY".bright + "} " +
        "count".underline + " " + "error".underline
    } 
    INFO
    exit
  end
end

PeopleGenerator.new
