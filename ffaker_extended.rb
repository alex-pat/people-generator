# encoding: utf-8

require "ffaker"

def file_to_array (file)
  File.readlines(file).map { |line| line.delete "\n" }
end

module FFaker
  module Address
    @cities_file = "us_cities.dat".freeze

    def load_db
      @database = file_to_array @cities_file
    end

    def city_zip_code
      FFaker.numerify @database.sample
    end

    def phone
      "+1-%010d" % rand(10**11)
    end
  end

  module AddressRU
    @cities_file = "ru_cities.dat".freeze

    def phone
      "+7-%010d" % rand(10**11)
    end
  end

  module AddressBY
    include FFaker::Address

    extend ModuleUtils
    extend self

    @cities_file = "by_cities.dat".freeze
    @streets_base = file_to_array "by_streets.dat"
    MAX_STREETS = 42

    def street_address
      "#{@streets_base.sample}, д. #{rand MAX_STREETS}"
    end

    def phone
      "+375-%09d" % rand(10**10)
    end

  end

  module NameBY
    include FFaker::Name

    extend ModuleUtils
    extend self

    LAST_NAMES = {
        male: file_to_array("by_male.dat").freeze,
        female: file_to_array("by_female.dat").freeze
    }.freeze

    FIRST_NAMES = {
        male: file_to_array("by_male_fname.dat").freeze,
        female: file_to_array("by_female_fname.dat").freeze
    }.freeze

    PATRONYMICS = {
        male: file_to_array("by_patr_male.dat").freeze,
        female: file_to_array("by_patr_female.dat").freeze
    }.freeze

    GENDER = [:male, :female].freeze

    def name
      sex = GENDER.sample
      "#{LAST_NAMES[sex].sample} #{FIRST_NAMES[sex].sample} #{PATRONYMICS[sex].sample}"
    end
  end
end
